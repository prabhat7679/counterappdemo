import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './CounterSlice'

export function Counter() {
  const count = useSelector((state) => state.counter.value)
  const dispatch = useDispatch()

  return (
    <div style={{
        display:"flex",
        fontSize:"30px",
        justifyContent:"center",
        alignItems:"center",
        gap:"3rem"
    }}>
    
        <button style={{fontSize:"30px"}}
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button style={{fontSize:"30px"}}
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
   
  )
}